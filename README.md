# Bienvenue sur le projet de documentation de Unity MARS!

Vous trouverez ici des exemples d'utilisation du module Unity MARS que vous pourrez tester pour avoir une idée de ses capacités et des différentes fonctionnalités de réalité augmentée qu'il implémente.

Une licence d'utilisation de Unity MARS est obligatoire afin de pouvoir utiliser les fonctionnalités du module dans le projet fourni en exemple.

Pour vous aider à installer ce module, le configurer et comprendre son fonctionnement, de la documentation a été rédigée dans la section [Wiki](https://gitlab.com/cimmi/interne/unity/exemples-unity-mars/-/wikis/home) de ce projet.

Il est à noter qu'en date du 25 mai 2021, la version la plus récente de Unity MARS est la 1.3.1 et que c'est celle-ci qui est utilisée dans les différents exemples dans ce projet.

À noter également que le projet a été réalisé avec la version 2019.4 LTS de l'éditeur Unity, mais que le module et le projet lui-même peuvent être utilisés avec des versions plus récentes de l'éditeur.

### Fonctionnalités implémentées dans la version 1.3.1 de Unity MARS

- Détection de plans (autant verticaux qu'horizontaux et permet leur visualisation)
- Les nuages de points
- Le Face Tracking
- Le Body Tracking (Utilisable uniquement avec ARKit sur la plateforme iOS pour le moment)
- La reconnaissance de marqueurs (Image Markers Tracking)

Je vous souhaite une bonne exploration de Unity MARS et de la documentation que je vous ai rédigé!

Je suis ouvert à tous commentaires et à toutes suggestions d'améliorations à apporter et suis disponible pour répondre à vos questionnements au sujet du module.

#### Rédigé par

Marc-Antoine Sigouin
